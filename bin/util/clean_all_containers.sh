#!/bin/bash

set -e

docker rm -f $(docker ps -aq) || true
docker rmi -f $(docker images | grep dev-peer | awk '{print $3}') || true
