docker exec peer0.org1.example.com \
peer chaincode invoke -o orderer.example.com:7050 -C mychannel -n BotnetDetectionContract \
--peerAddresses peer0.org1.example.com:7051 -c '{"Args":["addSingleNetworkFlow","99999999999999999999","123.0.0.1", "2.3.4.5", "tcp", "100", "50"]}'

sleep 2

docker exec peer0.org1.example.com \
peer chaincode query -C mychannel -n BotnetDetectionContract -c '{"Args":["getNetworkFlowsForKey","99999999999999999999-001"]}'

docker exec peer0.org1.example.com \
peer chaincode query -C mychannel -n BotnetDetectionContract -c '{"Args":["getDetectionState"]}'
