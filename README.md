Hyperledger Fabric network for botnet detection

# Requirements:
* hyperledger fabric 1.4 requirements https://hyperledger-fabric.readthedocs.io/en/release-1.4/getting_started.html
* nodejs >10
  * install js libs with "npm install" or "yarn install" into application/ folder
* java 8
* chaincode from https://gitlab.com/lucalanda/botnetdetectionchaincode pulled under contract/ folder
* unix based OS for scripts (some bash shell for windows could work fine, but I have not tested it)

# Project structure
* application/
  * e2e tests for basic contract functionalities of network flows insertions
  * "invoke.js" script for main botnet detection functionalities, it contains two transaction calls (one of them commented) for retrieving current detection state or performing botnet detection
  * "upload_peerrush_dataset.js" for uploading peerrush dataset (from dataset/ subfolder)
* bin/
  * scripts and binaries for blockchain network creation 
  * bin/util/
    * utilities scripts for network/docker maintenance
* contract/
  * java contract for network flows update, botnet detection, and all related code
* gateway/ and identity/ 
  * they contain metadata related to the current network structure, used by js scripts for connecting to it (they will be updated as soon as the structure will be expanded to multiple hosts)
* network/
  * crypto-config and configtx standard metadata for defining the blockchain structure, and related docker-compose.yml
* start.sh
  * creates a full instance of the network, installing the contract and testing that i works
* initialize.sh
  * useful for crypto-config and/or configtx meta data update, it should not be used unless it is necessary for updating the network structure (e.g. for adding some peers)

# Algorithm notes
PeerHunter is a static algorithm, that just takes all data as input and does not have intermediate states to start from. I have implemented my own version of PeerHunter, customized to return these intermediate states (which will be saved on the ledger) and to take them as input.
The algorithm customization is almost done, a minor issue for starting from the intermediate state should be fixed, but I consider it well tested (with all edge cases I could think of mapped to automatic unit/e2e tests).

The algorithm works with network flows data, which are tuples in the form of <ipSource, ipDestination, protocol, bytesPerPacketIn, bytesPerPacketOut> which can be extracted from .pcap datasets (e.g. using ARGUS) or they can be found in this form already in some other dataset (as the one included)

# Short description
This project uses a Fabric blockchain for botnet detection, based on PeerHunter algorithm.
It contains a pre-built .jar of the detection algorithm, and will automatically download all dependencies with gradle when starting the network.

The current network structure has the minimum amount of components to test it, with just one organization, one peer and one orderer. As soon as I will finish fixing the algorithm 

The contract under "contract/" allows network flows insertions/query and the execution of the botnet detection algorithm.

All the intermediate PeerHunter data, and the network flows data, will be compressed on the ledger, for performance sake in the read operations.
Anyway, they still require a lot of space, and currently the contract can execute reads from the ledger of up to 4MB, so the detection date is splitted in chunks before saving on the ledger.

# Example execution
* "./start.sh"
* "node application/upload_peerrush_dataset.sh"
* check that "application/invoke.js" has the contract call for "performBotnetDetection" uncommented (I'm sorry I have not fixed this script yet, I just used it for debugging until now)
* "node application/invoke.js"
if the upload_peerrush_dataset.sh was untouched (and so, it did upload the first 150k network flows), the execution of the algorithm should retrieve 3 bots (and the script "invoke.js" will output them)
* to easily kill the network, you can use "bin/util/clean_all_containers.sh", but note that it will also remove all other docker containers

# PLEASE NOTE
* for the ledger reads size problem i mentioned in the "Short description" section, I'm not sure that a detection on the full given dataset would work fine (if that does not happen, it will be because all the network flows records added to the ledger are more than 4MB grouped. I will check this possibility soon, for now the execution described in the "Example execution" works just fine).
